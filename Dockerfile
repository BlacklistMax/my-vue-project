FROM node:16.14.2 as dependency
WORKDIR /usr/src/app


COPY . ./

FROM dependency as build

WORKDIR /usr/src/app
RUN yarn install
RUN yarn run build
RUN yarn generate

ENV NODE_ENV='production'
ENV NUXT_HOST=0.0.0.0
ENV NUXT_PORT=3000

EXPOSE 3000

CMD ["yarn", "run", "start"]
