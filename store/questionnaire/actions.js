export default {
  async get(_, { companyId, branchId, agencyId }) {
    const { data } = await this.$axios.get('setup/questionnaires', {
      params: { companyId, branchId, agencyId }
    })

    return data
  },
  update(_, data) {
    return this.$axios.post('setup/questionnaires', data)
  },
}
