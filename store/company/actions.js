import { SET_COMPANIES, SET_COMPANY } from "./mutations";

export default {
  async get({ commit }, { status = null } = {}) {
    const { data } = await this.$axios.get("companies", {
      params: { status }
    });
    commit(SET_COMPANIES, data);

    return data;
  },
  async find({ commit }, id) {
    const { data } = await this.$axios.get(`companies/${id}`);
    commit(SET_COMPANY, data);

    return data;
  },
  update(_, { id, data }) {
    return this.$axios.put(`companies/${id}`, data);
  },
  async super_get({ commit }, { comapany = null } = {}) {
    const { data } = await this.$axios.get("admin/company", {
      params: { comapany }
    });
    commit(SET_COMPANIES, data);

    return data;
  }
};
