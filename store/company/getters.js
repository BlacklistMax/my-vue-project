export default {
  companies: ({ companies }) => companies,
  company: ({ company }) => company,
}
