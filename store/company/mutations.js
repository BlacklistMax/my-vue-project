export const SET_COMPANIES = 'SET_COMPANIES'
export const SET_COMPANY = 'SET_COMPANY'

export default {
  [SET_COMPANIES] (state, companies) {
    Object.assign(state, { companies })
  },
  [SET_COMPANY] (state, company) {
    Object.assign(state, { company })
  },
}
