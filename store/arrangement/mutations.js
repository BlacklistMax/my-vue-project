export const SET_ARRANGEMENTS = 'SET_ARRANGEMENTS'
export const SET_ARRANGED_ARRAGEMENTS = 'SET_ARRANGED_ARRAGEMENTS'

export default {
  [SET_ARRANGEMENTS] (state, arrangements) {
    Object.assign(state, { arrangements })
  },
  [SET_ARRANGED_ARRAGEMENTS] (state, arrangedArrangements) {
    Object.assign(state, { arrangedArrangements })
  }
}
