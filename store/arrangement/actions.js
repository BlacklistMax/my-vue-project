import { SET_ARRANGEMENTS, SET_ARRANGED_ARRAGEMENTS } from './mutations'

export default {
  async get({ commit }) {
    const { data } = await this.$axios.get('arrangements')
    commit(SET_ARRANGEMENTS, data)
  },
  async getArranged({ commit }) {
    const { data } = await this.$axios.get('arrangements/arranged')

    commit(SET_ARRANGED_ARRAGEMENTS, data)
  },
  arrange(_, data) {
    return this.$axios.post('arrangements', data)
  }
}
