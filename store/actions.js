import { format } from "date-fns";
import { SET_TITLE, SET_BUTTON, SET_PERMISSIONS } from "./mutations";

export default {
  async getPermissions({ commit }) {
    const { data } = await this.$axios.get("me/permissions");

    commit(SET_PERMISSIONS, data);
  },
  async getProvinces() {
    const { data } = await this.$axios.get("districts/provinces");

    return data;
  },
  async getAmphoes(_, province) {
    const { data } = await this.$axios.get("districts/amphoes", {
      params: { province }
    });

    return data;
  },
  async getDistricts(_, { province, amphoe }) {
    const { data } = await this.$axios.get("districts", {
      params: { province, amphoe }
    });

    return data;
  },
  async getZipCodes(_, { province, amphoe, district }) {
    const { data } = await this.$axios.get("zipcode", {
      params: { province, amphoe, district }
    });

    return data;
  },
  updateMyCompany(_, data) {
    return this.$axios.patch(`/companies/${this.$auth.user.companyId}`, data);
  },
  async userLogin({ commit }, payload) {
    const { username, password, type } = payload;

    const formData = {
      data: {
        username,
        password,
        type
      }
    };

    try {
      await this.$auth.loginWith("local", formData).then((res) => {
        if(!res.data.user.email_verified_at){
          this.$router.push({name: "change-password"});
        }else{
          this.$router.push({name: 'index'});
        }
      });

    } catch (error) {
      let message;
      switch (error?.response?.data?.code) {
        case "not-found-user":
          message = "หมายเลขโทรศัพท์ไม่ถูกต้องหรือไม่มีผู้ใช้นี้อยู่ในระบบ!";
          break;
        case "not-found-admin-user":
          message = "หมายเลขโทรศัพท์ไม่ถูกต้องหรือไม่มีผู้ใช้นี้อยู่ในระบบ!";
          break;
        case "user-is-disabled":
          message =
            "อีเมลหรือรหัสผ่านไม่ถูกต้อง! หรือไม่มีผู้ใช้นี้อยู่ในระบบ!";
          break;
        case "not-have-permission":
          message = "ผู้ใช้นี้ถูกปิดใช้งานแล้ว กรุณาติดต่อผู้ดูแลระบบ!";
          break;
        default:
          message = `ERROR: ${error?.response?.data?.message}`;
          break;
      }
      this.$toast.global.error({
        message
      });
    }

    commit("SET_LOGIN_LOADING", false);
  },
  isActionAllowed(_, actions) {
    const permissions = (this.$auth?.user?.accessPermissions || []).map(
      ({ code }) => code
    );

    const allowed = (Array.isArray(actions)
      ? actions
      : [actions]
    ).some(action => permissions.includes(action));

    return allowed;
  },
  async setTitle({ commit }, title) {
    commit(SET_TITLE, title);
  },
  async setButton({ commit }, button) {
    commit(SET_BUTTON, button);
  },
  async pad(_, n) {
    return n < 10 ? "0" + n : n;
  },
  async dateFormat({ dispatch }, { date, formatDate, isYearTH = true }) {
    if (!date) {
      date = new Date();
    }

    if (!formatDate) {
      formatDate = "yyyy-MM-dd";
    }

    const lang = await dispatch("getLang");

    if (lang === "th") {
      if (isYearTH) {
        const year = (date.getFullYear() + 543).toString();
        const month = date.getMonth() + 1;
        return `${await dispatch("pad", date.getDate())}/${await dispatch(
          "pad",
          month
        )}/${year}`;
      }
    }
    return format(date, formatDate);
  },
  async disabledHoursLessThanHour({ dispatch }, timeString) {
    if (timeString) {
      const hour = timeString.split(":")[0];
      const number = Number(hour);

      let disabledHours = [];

      for (let i = 0; i < number; i++) {
        const num = await dispatch("pad", i);
        disabledHours.push(num);
      }
      return disabledHours;
    }

    return [];
  },
  async datetimeFormat({ dispatch }, { datetime, formatDatetime }) {
    if (!datetime) {
      datetime = new Date();
    }

    if (!formatDatetime) {
      formatDatetime = "yyyy-MM-dd";
    }

    let formatTime = "HH:mm:ss";

    const lang = await dispatch("getLang");

    if (lang === "th") {
      const year = (datetime.getFullYear() + 543).toString().substring(2, 4);
      const month = datetime.getMonth() + 1;
      return `${await dispatch("pad", datetime.getDate())}/${await dispatch(
        "pad",
        month
      )}/${year} ${format(datetime, formatTime)}`;
    }

    return format(datetime, formatDatetime);
  },
  async timeFormat(_, { time, formatTime }) {
    // console.log("time =>", time);

    if (!time) {
      time = new Date();
    }

    if (!formatTime) {
      formatTime = "HH:mm";
    }

    return format(time, formatTime);
  },
  numberWithCommas(_, x) {
    if (!x) return "0";

    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  },
  getLang() {
    return "th";
  },
  async logout() {
    await this.$auth.logout();
    this.$router.push("/login");
  },
  onlyNumber($event) {
    let keyCode = $event.keyCode ? $event.keyCode : $event.which;
    if ((keyCode < 48 || keyCode > 57) && keyCode !== 46) {
      // 46 is dot
      $event.preventDefault();
    }
  },
  formatPhoneNumber(phoneNumber) {
    if (phoneNumber.length !== 0) {
      if (phoneNumber.length === 10 || phoneNumber.length === 12) {
        // format เบอร์มือถือ 10หลัก หรือถูกแปลงเป็นเบอร์บ้าน 9หลัก
        // ถ้าพิมพ์เพิ่มจาก 9หลัก เป็น 10หลัก จะแปลงเป็น 000-000-0000

        var cleaned = ("" + phoneNumber).replace(/\D/g, "");
        phoneNumber = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/);
        this.phoneNumber =
          phoneNumber[1] + "-" + phoneNumber[2] + "-" + phoneNumber[3];
      } else if (phoneNumber.length === 9 || phoneNumber.length === 11) {
        // format เบอร์บ้าน(กทม 02-123-1234) 9หลัก
        // ถ้าพิมพ์ลบจาก 9หลัก เป็น 10หลัก จะแปลงเป็น 000-000-0000

        var cleaned = ("" + phoneNumber).replace(/\D/g, "");
        phoneNumber = cleaned.match(/^(\d{2})(\d{3})(\d{4})$/);
        this.phoneNumber =
          phoneNumber[1] + "-" + phoneNumber[2] + "-" + phoneNumber[3];
      }
      return this.phoneNumber;
    }
  }
};
