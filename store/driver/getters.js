export default {
  drivers: ({ drivers }) => drivers,
  driver: ({ driver }) => driver,
}
