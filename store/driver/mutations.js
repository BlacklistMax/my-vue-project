export const SET_DRIVERS = 'SET_DRIVERS'
export const SET_DRIVER = 'SET_DRIVER'

export default {
  [SET_DRIVERS] (state, drivers) {
    Object.assign(state, { drivers })
  },
  [SET_DRIVER] (state, driver) {
    Object.assign(state, { driver })
  }
}
