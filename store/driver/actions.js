import { SET_DRIVERS, SET_DRIVER } from './mutations'

export default {
  async get({ commit }) {
    const { data } = await this.$axios.get('drivers')
    commit(SET_DRIVERS, data)
  },
  create(_, body) {
    return this.$axios.post('drivers', body)
  },
  async find({ commit }, id) {
    const { data } = await this.$axios.get(`drivers/${id}`)
    commit(SET_DRIVER, data)
  },
  update(_, body) {
    return this.$axios.patch(`drivers/${body.id}`, body)
  },
  upload(_, { id, file }) {
    const formData = new FormData()
    formData.append('file', file)

    return this.$axios.post(`drivers/${id}/photo`, formData)
  },
  remove(_, id) {
    return this.$axios.delete(`drivers/${id}`)
  }
}
