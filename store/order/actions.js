import { SET_ORDERS, SET_ORDER, SET_EVENTS, SET_YEAR } from './mutations'

export default {
  async get({ commit }) {
    const { data } = await this.$axios.get('orders')
    commit(SET_ORDERS, data)
  },
  async find({ commit }, id) {
    const { data } = await this.$axios.get(`orders/${id}`)
    commit(SET_ORDER, data)

    return data
  },
  create(_, data) {
    return this.$axios.post('orders', data)
  },
  restore(_, { id, data }) {
    return this.$axios.put(`orders/${id}/restore`, data)
  },
  async createFuel(_, { id, fuel }) {
    // console.log(id, fuel);
    const { data } = await this.$axios.post(`orders/${id}/fuels`, fuel)
    return data
  },
  uploadReceipt(_, { id, fuelId, receipt }) {
    const formData = new FormData()
    formData.append('file', receipt)

    return this.$axios.post(`orders/${id}/fuels/${fuelId}/receipt`, formData)
  },
  async getEvents({ commit }, { plateNumber= '', startDate='', finishDate=''} = {}) {
    const today = new Date();
    // filterYear = '2022';
    // filterYear = filterYear == '' ? today.getFullYear() : filterYear;
    const { data } = await this.$axios.get('events', {
      params: { plateNumber, startDate, finishDate}
    })
    commit(SET_EVENTS, data)
  },
  async getEventYear({ commit }){
    const {data} = await this.$axios.get('event/year')
    commit(SET_YEAR, data)
  },
  cancelOrder(_, { id, data}) {
    return this.$axios.post(`orders/cancel/${id}`);
  },
  redoOrder(_, { id, data }) {
    return this.$axios.post(`orders/redo/${id}`);
  }

}
