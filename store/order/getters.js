export default {
  orders: ({ orders }) => orders,
  order: ({ order }) => order,
  events: ({ events }) => events,
  eventsYear: ({ eventsYear}) => eventsYear
};
