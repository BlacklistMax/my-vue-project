export const SET_ORDERS = 'SET_ORDERS'
export const SET_ORDER = 'SET_ORDER'
export const SET_EVENTS = 'SET_EVENTS'
export const SET_YEAR = 'SET_YEAR'

export default {
  [SET_ORDERS] (state, orders) {
    Object.assign(state, { orders })
  },
  [SET_ORDER] (state, order) {
    Object.assign(state, { order })
  },
  [SET_EVENTS] (state, events) {
    Object.assign(state, { events })
  },
  [SET_YEAR] (state, eventsYear) {
    Object.assign(state, { eventsYear })
  }
}
