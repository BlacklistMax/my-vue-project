export const SET_PERMISSIONS = 'SET_PERMISSIONS'
export const SET_PERMISSION = 'SET_PERMISSION'
export const UNSET_PERMISSION = 'UNSET_PERMISSION'

export default {
  [SET_PERMISSIONS] (state, permissions) {
    Object.assign(state, { permissions })
  },
  [SET_PERMISSION] (state, permission) {
    Object.assign(state, { permission })
  },
  [UNSET_PERMISSION] (state) {
    state.permission = null
  }
}
