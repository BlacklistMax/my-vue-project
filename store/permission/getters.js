export default {
  permissions: ({ permissions }) => permissions,
  permission: ({ permission }) => permission,
  resources: ({ resources }) => resources,
  allowedResources: ({ resources }) =>
    resources.filter(({ view, create, edit, remove }) =>
      [view, create, edit, remove].some(action => action !== null)
    )
}
