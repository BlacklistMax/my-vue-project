import { SET_PERMISSIONS, SET_PERMISSION, UNSET_PERMISSION } from "./mutations";

export default {
  async get({ commit }) {
    const { data } = await this.$axios.get("permissions");
    commit(SET_PERMISSIONS, data);
  },
  async admin_get({ commit }) {
    const { data } = await this.$axios.get("admin/permission");
    commit(SET_PERMISSIONS, data);
  },
  async find({ commit }, id) {
    commit(UNSET_PERMISSION);
    const { data } = await this.$axios.get(`permissions/${id}`);
    commit(SET_PERMISSION, data);
  },
  create(_, { name, actions, companyId }) {
    return this.$axios.post("permissions", { name, actions, companyId });
  },
  update(_, { id, name, actions }) {
    return this.$axios.put(`permissions/${id}`, { name, actions });
  },
  delete(_, id) {
    return this.$axios.delete(`permissions/${id}`);
  }
};
