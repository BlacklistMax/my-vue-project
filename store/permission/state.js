export default () => ({
  permissions: [],
  permission: null,
  resources: [
    {
      name: "บันทึกการขอใช้รถยนต์",
      view: "request_view",
      create: "request_create",
      edit: "request_edit",
      remove: null,
      actions: []
    },
    {
      name: "สรุปการใช้รถยนต์ (ราย วัน | เดือน)",
      view: "usage_view",
      create: "usage_create",
      edit: null,
      remove: null,
      actions: []
    },
    {
      name: "อนุมัติ",
      view: "approval_view",
      create: "approval_create",
      edit: null,
      remove: null,
      actions: []
    },
    {
      name: "จัดรถยนต์",
      view: "management_view",
      create: "management_create",
      edit: null,
      remove: null,
      actions: []
    },
    {
      name: "ข้อมูลรถยนต์",
      view: "car_view",
      create: "car_create",
      edit: null,
      remove: null,
      actions: []
    },
    {
      name: "แจ้งซ่อม",
      view: "repair_view",
      create: "repair_create",
      edit: null,
      remove: null,
      actions: []
    },
    {
      name: "รายงาน",
      view: "report_view",
      create: null,
      edit: null,
      remove: null,
      actions: []
    },
    {
      name: "ปฎิทิน",
      view: "calendar_view",
      create: null,
      edit: null,
      remove: null,
      actions: []
    },
    {
      name: "ตั้งค่าบริษัท",
      view: "company_view",
      create: "company_create",
      edit: "company_edit",
      remove: "company_remove",
      actions: []
    },
    {
      name: "ตั้งค่าสาขา",
      view: "branch_view",
      create: "branch_create",
      edit: "branch_edit",
      remove: "branch_remove",
      actions: []
    },
    {
      name: "ตั้งค่าแผนก",
      view: "agency_view",
      create: "agency_create",
      edit: "agency_edit",
      remove: "agency_remove",
      actions: []
    },
    {
      name: "ตั้งค่าสัญญา",
      view: "contract_view",
      create: "contract_create",
      edit: "contract_edit",
      remove: "contract_remove",
      actions: []
    },
    {
      name: "ตั้งค่าทรัพย์สิน",
      view: "asset_view",
      create: "asset_create",
      edit: "asset_edit",
      remove: "asset_remove",
      actions: []
    },
    {
      name: "ตั้งค่าการอนุมัติ",
      view: "approval_view",
      create: "approval_create",
      edit: null,
      remove: null,
      actions: []
    },
    {
      name: "ตั้งค่าผู้ใช้",
      view: "user_view",
      create: "user_create",
      edit: "user_edit",
      remove: "user_remove",
      actions: []
    },
    {
      name: "ตั้งค่าคำถามประเมิน",
      view: "question_view",
      create: "question_create",
      edit: "question_edit",
      remove: "question_remove",
      actions: []
    },
    {
      name: "ตั้งค่าสิทธิการใช้งาน",
      view: "permission_view",
      create: "permission_create",
      edit: "permission_edit",
      remove: "permission_remove",
      actions: []
    },
    {
      name: "ตั้งค่ายี่ห้อ",
      view: "brand_view",
      create: "brand_create",
      edit: "brand_edit",
      remove: "brand_remove",
      actions: []
    },
    {
      name: "ตั้งค่าสี",
      view: "color_view",
      create: "color_create",
      edit: "color_edit",
      remove: "color_remove",
      actions: []
    },
    {
      name: "ตั้งค่าพนักงานขับ",
      view: "driver_view",
      create: "driver_create",
      edit: "driver_edit",
      remove: "driver_remove",
      actions: []
    },
    {
      name: "Admin Company",
      view: "all_view",
      create: "all_create",
      edit: "all_edit",
      remove: "all_remove",
      actions: []
    },
    {
      name: "ตั้งค่าทุกหน่วยงาน",
      view: "manages_all",
      create: null,
      edit: null,
      remove: null,
      actions: []
    }
  ]
});
