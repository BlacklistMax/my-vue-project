export const SET_ASSETS = "SET_ASSETS";
export const SET_ASSET = "SET_ASSET";
export const SET_MAX_PAGE = "SET_MAX_PAGE";
export const SET_LOADING = "SET_LOADING";
export default {
  [SET_ASSETS](state, assets) {
    Object.assign(state, { assets });
  },
  [SET_ASSET](state, asset) {
    if (asset) {
      asset.cars = asset.cars.map((item, index) => {
        return {
          ...item,
          no: index + 1
        };
      });
    }
    Object.assign(state, { asset });
  },
  [SET_MAX_PAGE](state, max_page) {
    Object.assign(state, { max_page });
  },
  [SET_LOADING](state, loading) {
    Object.assign(state, { loading });
  },
};
