import {
  SET_ASSETS,
  SET_ASSET,
  SET_MAX_PAGE,
  SET_LOADING
} from './mutations'

export default {
  async get({ commit }, { search = "", companyId = null } = {}) {
    const { data } = await this.$axios.get('setup/assets', {
      params: { companyId, search }
    })

    commit(SET_ASSETS, data)
  },
  create(_, body) {
    return this.$axios.post('setup/assets', body)
  },
  async find({ commit }, id) {
    commit(SET_ASSET, null)
    const { data } = await this.$axios.get(`setup/assets/${id}`)
    commit(SET_ASSET, data)
  },
  update(_, { id, data }) {
    return this.$axios.put(`setup/assets/${id}`, data)
  },
  remove(_, id) {
    return this.$axios.delete(`setup/assets/${id}`)
  },

  async getCars({ commit }, { search = "", searchNumberPlate = "", searchDepartment = "", searchType = "", page = "", itemsPerPage = "" } = {}) {
    commit(SET_LOADING, true);
    commit(SET_ASSETS, []);
    commit(SET_MAX_PAGE, 0);
    const { data } = await this.$axios.get(`setup/assets?page=${page}&per_page=${itemsPerPage}`, {
      params: {
        search,
        searchNumberPlate,
        searchDepartment,
        searchType
      }
    });
    commit(SET_LOADING, false);
    commit(SET_ASSETS, data.data);
    commit(SET_MAX_PAGE, data.last_page);
  }
}
