export default {
  assets: ({ assets }) => assets,
  asset: ({ asset }) => asset,
  max_page: ({ max_page }) => max_page,
  loading: ({ loading }) => loading,
}
