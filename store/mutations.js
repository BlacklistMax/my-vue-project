export const SET_TITLE = 'SET_TITLE'
export const SET_BUTTON = 'SET_BUTTON'
export const UNSET_BUTTONS = 'UNSET_BUTTONS'
export const SET_LOGIN_LOADING = 'SET_LOGIN_LOADING'
export const SET_PERMISSIONS = 'SET_PERMISSIONS'

export default {
  [SET_TITLE](state, title) {
    Object.assign(state, { title })
  },
  [SET_BUTTON]({ buttons }, button) {
    buttons.push(button)
  },
  [SET_PERMISSIONS](state, permissions) {
    Object.assign(state, { permissions })
  },
  [UNSET_BUTTONS](state) {
    Object.assign(state, { buttons: [] })
  },
  [SET_LOGIN_LOADING](state, isLoginLoading) {
    Object.assign(state, { isLoginLoading })
  },
}
