export default {
  agencies: ({ agencies }) => agencies,
  agency: ({ agency }) => agency,
}
