import { SET_AGENCIES, SET_AGENCY } from './mutations'

export default {
  async get({ commit }, { status = null } = {}) {
    const { data } = await this.$axios.get('agencies', {
      params: { status }
    })

    commit(SET_AGENCIES, data)
  },
  async admin_get({ commit }, { status = null } = {}) {
    const { data } = await this.$axios.get('/admin/agency', {
      params: { status }
    })

    commit(SET_AGENCIES, data)
  },
  async getById({ commit }, { branchId = null,  status = null } = {}) {
    const { data } = await this.$axios.get(`/agencies/branch/${branchId}`, {
      params: { status }
    })

    commit(SET_AGENCIES, data)
  },
  create(_, body) {
    return this.$axios.post('agencies', body)
  },
  async find({ commit }, id) {
    const { data } = await this.$axios.get(`agencies/${id}`)
    commit(SET_AGENCY, data)
  },
  update(_, { id, branchId, name, responsibleUserId }) {
    return this.$axios.patch(`agencies/${id}`, {
      branchId,
      name,
      responsibleUserId
    })
  },
  remove(_, id) {
    return this.$axios.delete(`agencies/${id}`)
  }
}
