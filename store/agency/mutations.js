export const SET_AGENCIES = 'SET_AGENCIES'
export const SET_AGENCY = 'SET_AGENCY'

export default {
  [SET_AGENCIES] (state, agencies) {
    Object.assign(state, { agencies })
  },
  [SET_AGENCY] (state, agency) {
    Object.assign(state, { agency })
  }
}
