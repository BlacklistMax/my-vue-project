export const SET_QUESTIONS = 'SET_QUESTIONS'

export default {
  [SET_QUESTIONS] (state, questions) {
    Object.assign(state, { questions })
  },
}
