import { SET_QUESTIONS } from './mutations'

export default {
  async get({ commit }) {
    const { data } = await this.$axios.get('questions')
    commit(SET_QUESTIONS, data)
  },
}
