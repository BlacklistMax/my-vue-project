export default () => ({
  contracts: [],
  contract: null,
  maintenances: [],
  maintenance: null,
  orders: [],
  max_page: 0,
  order: null,
  exportorders: null,
  carbycontract: [],
  cardetail: [],
  users:[],
  user:null,
  oils:[],
  cars:[{1:null}],
  oil:null,
  loading: false
});
