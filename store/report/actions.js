import {
  SET_ORDERS,
  SET_ORDER,
  SET_MAINTENANCES,
  SET_MAINTENANCE,
  SET_CONTRACTS,
  SET_CONTRACT,
  SET_EXPORT_ORDERS,
  SET_CAR_CONTRACT,
  SET_CAR_DETAIL,
  SET_USERS,
  SET_USER,
  SET_OILS,
  SET_CARS,
  SET_OIL,
  SET_MAX_PAGE,
  SET_LOADING
} from "./mutations";

export default {
  async getOrder({ commit }, { carId = null, dateRange = null, page = 1, itemsPerPage = 10, selectTypeCar = 1} = {}) {
    commit(SET_ORDERS, []);
    commit(SET_LOADING, true);
    commit(SET_MAX_PAGE, 0);
    const { data } = await this.$axios.get(`report/order?page=${page}&per_page=${itemsPerPage}&selectTypeCar=${selectTypeCar}`, {
      params: { carId, dateRange }
    });
    commit(SET_LOADING, false);
    commit(SET_ORDERS, data.data);
    commit(SET_MAX_PAGE, data.last_page);
  },

  async exportOrder({ commit }, { search = "", status = null } = {}) {
    commit(SET_EXPORT_ORDERS, null);

    const { data } = await this.$axios.get(`export/order`, {
      params: { status, search }
    });
    commit(SET_EXPORT_ORDERS, data);
  },

  async getMaintenance({ commit }, { carId = null, contractId = null, page = 1, itemsPerPage = 10} = {}) {
    commit(SET_MAINTENANCES, []);
    commit(SET_LOADING, true);
    commit(SET_MAX_PAGE, 0);

    const { data } = await this.$axios.get(`report/maintenance?page=${page}&per_page=${itemsPerPage}`, {
      params: { carId, contractId }
    });
    commit(SET_MAINTENANCES, data.data);
    commit(SET_LOADING, false);
    commit(SET_MAX_PAGE, data.last_page);
  },
  async findMantenance({ commit }, id) {
    commit(SET_MAINTENANCE, null);

    const { data } = await this.$axios.get(`repair/maintenance/${id}`);
    commit(SET_MAINTENANCE, data);
  },
  async getContracts({ commit }, { search = null, status = null, page = 1, itemsPerPage = 10} = {}) {
    commit(SET_LOADING, true);
    commit(SET_CONTRACTS, []);
    commit(SET_MAX_PAGE, 0);
    const { data } = await this.$axios.get(`report/contract?page=${page}&per_page=${itemsPerPage}`, {
      params: { status, search }
    });
    commit(SET_LOADING, false);
    commit(SET_CONTRACTS, data.data);
    commit(SET_MAX_PAGE, data.last_page);
  },
  async findContract({ commit }, id) {
    commit(SET_CONTRACT, null);

    const { data } = await this.$axios.get(`report/contract/${id}`);
    commit(SET_CONTRACT, data);
  },

  async getCarByContractId({ commit }, id) {
    commit(SET_CAR_CONTRACT, null);

    const { data } = await this.$axios.get(`report/contract/${id}/car`);
    commit(SET_CAR_CONTRACT, data);
  },
  async getCarDetail({ commit }, id) {
    commit(SET_CAR_DETAIL, null);
    const { data } = await this.$axios.get(`report/car/${id}`);

    commit(SET_CAR_DETAIL, data);
  },
  async getUsers({ commit }, { userId = null, page = 1, itemsPerPage = 10} = {}) {
    commit(SET_LOADING, true);
    commit(SET_MAX_PAGE, 0);
    commit(SET_USERS, []);

    const { data } = await this.$axios.get(`report/users?page=${page}&per_page=${itemsPerPage}`, {
      params: { userId}
    });
    commit(SET_USERS, data.data);
    commit(SET_LOADING, false);
    commit(SET_MAX_PAGE, data.last_page);
  },
  async getUser({ commit }, { id = null, page = 1, itemsPerPage = 10} = {}) {
    commit(SET_USER, null);
    commit(SET_LOADING, true);
    commit(SET_MAX_PAGE, 0);

    const { data } = await this.$axios.get(`report/users/${id}?page=${page}&per_page=${itemsPerPage}`);
    commit(SET_USER, data);
    commit(SET_LOADING, false);
    commit(SET_MAX_PAGE, data.request.last_page);
  },
  async getOils({ commit },{ carId = null, dateRange = null, userId = null, page = 1, itemsPerPage = 10} = {}) {
    commit(SET_OILS);
    commit(SET_LOADING, true);
    commit(SET_MAX_PAGE, 0);
    const { data } = await this.$axios.get(`report/oils?page=${page}&per_page=${itemsPerPage}`, {
      params: { userId,carId,dateRange}
    });
    commit(SET_OILS, data.data);
    commit(SET_LOADING, false);
    commit(SET_MAX_PAGE, data.last_page);
  },
  async getCarsByOils({ commit }) {
    commit(SET_CARS, []);
    const { data } = await this.$axios.get(`report/cars/oils`);
    commit(SET_CARS, data);
  },
  async getOil({ commit }, id) {
    commit(SET_OIL, null);

    const { data } = await this.$axios.get(`report/oils/${id}`);
    commit(SET_OIL, data);
  },
};
