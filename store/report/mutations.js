export const SET_ORDERS = "SET_ORDERS";
export const SET_ORDER = "SET_ORDER";
export const SET_MAINTENANCES = "SET_MAINTENANCES";
export const SET_MAINTENANCE = "SET_MAINTENANCE";
export const SET_CONTRACTS = "SET_CONTRACTS";
export const SET_CONTRACT = "SET_CONTRACT";
export const SET_EXPORT_ORDERS = "SET_EXPORT_ORDERS";
export const SET_CAR_CONTRACT = "SET_CAR_CONTRACT";
export const SET_CAR_DETAIL = "SET_CAR_DETAIL";
export const SET_USERS = "SET_USERS";
export const SET_USER = "SET_USER";
export const SET_OILS = "SET_OILS";
export const SET_CARS = "SET_CARS";
export const SET_OIL = "SET_OIL";
export const SET_MAX_PAGE = "SET_MAX_PAGE";
export const SET_LOADING = "SET_LOADING";
export default {
  [SET_LOADING](state, loading) {
    Object.assign(state, { loading });
  },
  [SET_MAX_PAGE](state, max_page) {
    Object.assign(state, { max_page });
  },
  [SET_ORDERS](state, orders) {
    Object.assign(state, { orders });
  },
  [SET_ORDER](state, order) {
    Object.assign(state, { order });
  },
  [SET_MAINTENANCES](state, maintenances) {
    Object.assign(state, { maintenances });
  },
  [SET_MAINTENANCE](state, maintenance) {
    Object.assign(state, { maintenance });
  },
  [SET_CONTRACTS](state, contracts) {
    Object.assign(state, { contracts });
  },
  [SET_CONTRACT](state, contract) {
    Object.assign(state, { contract });
  },
  [SET_EXPORT_ORDERS](state, exportorders) {
    Object.assign(state, { exportorders });
  },
  [SET_CAR_CONTRACT](state, carbycontract) {
    Object.assign(state, { carbycontract });
  },
  [SET_CAR_DETAIL](state, cardetail) {
    Object.assign(state, { cardetail });
  },
  [SET_USERS](state, users) {
    Object.assign(state, { users });
  },
  [SET_USER](state, user) {
    Object.assign(state, { user });
  },
  [SET_OILS](state, oils) {
    Object.assign(state, { oils });
  },
  [SET_CARS](state, cars) {
    Object.assign(state, { cars });
  },
  [SET_OIL](state, oil) {
    Object.assign(state, { oil });
  }
};
