import {
  SET_REPAIRS,
  SET_REPAIR,

} from './mutations'

export default {
  async get({ commit }, { search= '', status = null } = {}) {
    commit(SET_REPAIRS, [])

    const { data } = await this.$axios.get(`repair`, {
      params: { status, search }
    })
    commit(SET_REPAIRS, data)
  },
  async find({ commit }, id) {
    commit(SET_REPAIR, null)

    const { data } = await this.$axios.get(`repair/${id}`)
    commit(SET_REPAIR, data)
  },
  create(_, body) {
    return this.$axios.post('repair/create', body)
  },
  update(_, body) {
    return this.$axios.post(`repair/update`, body)
  },
  remove(_, id) {
    return this.$axios.delete(`repair/${id}/remove`)
  }

}
