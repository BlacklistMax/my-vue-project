export const SET_REPAIRS = "SET_REPAIRS";
export const SET_REPAIR = "SET_REPAIR";



export default {
  [SET_REPAIRS](state, repairs) {
    Object.assign(state, { repairs });
  },
  [SET_REPAIR](state, repair) {
    Object.assign(state, { repair });
  }
};

