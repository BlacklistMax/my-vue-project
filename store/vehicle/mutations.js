import Vue from 'vue'

export const SET_CR_VEHICLE = 'SET_CR_VEHICLE'

export default {
    [SET_CR_VEHICLE](state, cr_vehicle) {
        Object.assign(state, { cr_vehicle })
    }
}