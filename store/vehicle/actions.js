
export default {
    async getcr_vehicle({ commit }) {
        const cr_vehicle = this.$axios.get('cr_vehicle')
        commit(SET_CR_VEHICLE,cr_vehicle.data)
    }
}
