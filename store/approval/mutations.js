export const SET_APPROVALS = 'SET_APPROVALS'
export const SET_APPROVAL = 'SET_APPROVAL'

export default {
  [SET_APPROVALS] (state, approvals) {
    Object.assign(state, { approvals })
  },
  [SET_APPROVAL] (state, approval) {
    Object.assign(state, { approval })
  }
}
