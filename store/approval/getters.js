export default {
  approvals: ({ approvals }) => approvals,
  approval: ({ approval }) => approval,
}
