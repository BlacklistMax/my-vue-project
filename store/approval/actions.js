import { SET_APPROVALS, SET_APPROVAL } from './mutations'

export default {
  async get({ commit }) {
    const { data } = await this.$axios.get('approvals')
    commit(SET_APPROVALS, data)

    return data
  },
  async find({ commit }, id) {
    const { data } = await this.$axios.get(`approvals/${id}`)
    commit(SET_APPROVAL, data)

    return data
  },
  approve(_, { id, data }) {
    return this.$axios.post(`approvals/${id}/approve`, data)
  }
}
