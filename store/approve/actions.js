export default {
  async get(_, { id, companyId, branchId, agencyId }) {
    const { data } = await this.$axios.get(`setup/approvals/${id}`, {
      params: { companyId, branchId, agencyId }
    })

    return data
  },
  update(_, { id, data }) {
    return this.$axios.put(`setup/approvals/${id}`, data)
  },
}
