export const SET_MASTER_GEAR = "SET_MASTER_GEAR";
export const SET_MASTER_REAPIR_TYPES = "SET_MASTER_REAPIR_TYPES";
export const SET_MASTER_FUEL = "SET_MASTER_FUEL";
export const SET_MASTER_VEHICEL_STATUS = "SET_MASTER_VEHICEL_STATUS";
export const SET_MASTER_COLOR = "SET_MASTER_COLOR";



export default {
  [SET_MASTER_GEAR](state, gear) {
    Object.assign(state, { gear });
  },
  [SET_MASTER_REAPIR_TYPES](state, repairtype) {
    Object.assign(state, { repairtype });
  },
  [SET_MASTER_FUEL](state, fuels) {
    Object.assign(state, { fuels });
  },
  [SET_MASTER_VEHICEL_STATUS](state, carstatus) {
    Object.assign(state, { carstatus });
  },
  [SET_MASTER_COLOR](state, color) {
    Object.assign(state, { color });
  }

};
