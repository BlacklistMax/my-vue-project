import {
  SET_MASTER_GEAR,
  SET_MASTER_REAPIR_TYPES,
  SET_MASTER_FUEL,
  SET_MASTER_VEHICEL_STATUS,
  SET_MASTER_COLOR
} from './mutations'

export default {
  async getMasterGear({ commit }, { search= '', status = null } = {}) {
    commit(SET_MASTER_GEAR, [])

    const { data } = await this.$axios.get('/master/gear', {
      params: { status, search }
    })
    commit(SET_MASTER_GEAR, data)
  },
  async getRepairType({ commit }, { search= '', status = null } = {}) {
    commit(SET_MASTER_REAPIR_TYPES, [])

    const { data } = await this.$axios.get('/master/repairtype', {
      params: { status, search }
    })
    commit(SET_MASTER_REAPIR_TYPES, data)
  },
  async getMasterFuels({ commit }, { search= '', status = null } = {}) {
    commit(SET_MASTER_FUEL, [])

    const { data } = await this.$axios.get('/master/fuel', {
      params: { status, search }
    })
    commit(SET_MASTER_FUEL, data)
  },
  async getMasterVehicleStatus({ commit }, { search= '', status = null } = {}) {
    commit(SET_MASTER_VEHICEL_STATUS, [])

    const { data } = await this.$axios.get('/master/vehicle-status', {
      params: { status, search }
    })
    commit(SET_MASTER_VEHICEL_STATUS, data)
  },
  async getMasterColor({ commit }, { search= '', status = null } = {}) {
    commit(SET_MASTER_COLOR, [])

    const { data } = await this.$axios.get('/master/color', {
      params: { status, search }
    })
    commit(SET_MASTER_COLOR, data)
  },



}
