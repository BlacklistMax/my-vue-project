export default {
  gear: ({ gear }) => gear,
  repairtype: ({ repairtype }) => repairtype,
  fuels: ({ fuels }) => fuels,
  carstatus: ({ carstatus }) => carstatus,
  color: ({color}) => color
};
