export default {
  brands: ({ brands }) => brands,
  brand: ({ brand }) => brand,
  models: ({ models }) => models,
}
