import { SET_BRANDS, SET_BRAND, SET_MODELS } from './mutations'

export default {
  async get({ commit }, { status = null } = {}) {
    const { data } = await this.$axios.get('setup/brands', {
      params: { status }
    })
    commit(SET_BRANDS, data)

    return data
  },
  async find({ commit }, id) {
    const { data } = await this.$axios.get(`setup/brands/${id}`)
    commit(SET_BRAND, data)

    return data
  },
  update(_, brands) {
    return this.$axios.put('setup/brands', { brands })
  },
  async getModels({ commit }, brandId) {
    const { data } = await this.$axios.get(`setup/brands/${brandId}/models`)
    commit(SET_MODELS, data)

    return data
  },
  updateModels(_, { brandId, models }) {
    return this.$axios.put(`setup/brands/${brandId}/models`, { models })
  }
}
