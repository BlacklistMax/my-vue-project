export const SET_BRANDS = 'SET_BRANDS'
export const SET_BRAND = 'SET_BRAND'
export const SET_MODELS = 'SET_MODELS'

export default {
  [SET_BRANDS] (state, brands) {
    Object.assign(state, { brands })
  },
  [SET_BRAND] (state, brand) {
    Object.assign(state, { brand })
  },
  [SET_MODELS] (state, models) {
    Object.assign(state, { models })
  }
}
