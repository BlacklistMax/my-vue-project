export default {
  users: ({ users }) => users,
  user: ({ user }) => user,
}
