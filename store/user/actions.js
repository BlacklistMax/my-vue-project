import { SET_USERS, SET_USER } from "./mutations";

export default {
  async get({ commit }) {
    const { data } = await this.$axios.get("users");
    commit(SET_USERS, data);

    return data;
  },
  async find({ commit }, id) {
    const { data } = await this.$axios.get(`users/${id}`);
    commit(SET_USER, data);

    return data;
  },
  async getApprovvals({ commit }, id) {
    const { data } = await this.$axios.get(`users/approvals`);
    return commit(SET_USERS, data);
  },
  create(_, data) {
    return this.$axios.post("users", data);
  },
  update(_, { id, data }) {
    return this.$axios.put(`users/${id}`, data);
  },
  delete(_, id) {
    return this.$axios.delete(`users/${id}`);
  },
  async Adminget({ commit }) {
    const { data } = await this.$axios.get("admin/user");
    commit(SET_USERS, data);

    return data;
  }
};
