export const SET_USERS = 'SET_USERS'
export const SET_USER = 'SET_USER'

export default {
  [SET_USERS] (state, users) {
    Object.assign(state, { users })
  },
  [SET_USER] (state, user) {
    Object.assign(state, { user })
  },
}
