export const SET_CARS = "SET_CARS";
export const SET_CAR = "SET_CAR";
export const SET_CAR_TYPES = "SET_CAR_TYPES";
export const SET_CAR_INFORMATION = "SET_CAR_INFORMATION";
export const SET_CAR_OWNER = "SET_CAR_OWNER";
export const SET_CAR_INSURANCES = "SET_CAR_INSURANCES";
export const SET_CAR_TAXES = "SET_CAR_TAXES";
export const SET_CAR_ACTS = "SET_CAR_ACTS";
export const SET_CAR_OPTIONS = "SET_CAR_OPTIONS";
export const SET_CAR_MAINTENANCE = "SET_CAR_MAINTENANCE";
export const SET_CAR_ERP_INFORMATION = "SET_CAR_ERP_INFORMATION";
export const SET_CAR_ERP_OWNER = "SET_CAR_ERP_OWNER";
export const SET_CAR_ERP_INSURANCES = "SET_CAR_ERP_INSURANCES";
export const SET_CAR_ERP_TAXES = "SET_CAR_ERP_TAXES";
export const SET_CAR_ERP_ACTS = "SET_CAR_ERP_ACTS";
export const SET_CAR_ERP_OPTIONS = "SET_CAR_ERP_OPTIONS";
export const SET_CAR_ERP_MAINTENANCE = "SET_CAR_ERP_MAINTENANCE";





export default {
  [SET_CARS](state, cars) {
    Object.assign(state, { cars });
  },
  [SET_CAR](state, car) {
    Object.assign(state, { car });
  },
  [SET_CAR_TYPES](state, carTypes) {
    Object.assign(state, { carTypes });
  },
  [SET_CAR_INFORMATION](state, information) {
    Object.assign(state, { information });
  },
  [SET_CAR_OWNER](state, owner) {
    Object.assign(state, { owner });
  },
  [SET_CAR_INSURANCES](state, insurances) {
    Object.assign(state, { insurances });
  },
  [SET_CAR_TAXES](state, taxes) {
    Object.assign(state, { taxes });
  },
  [SET_CAR_ACTS](state, acts) {
    Object.assign(state, { acts });
  },
  [SET_CAR_OPTIONS](state, options) {
    Object.assign(state, { options });
  },
  [SET_CAR_MAINTENANCE](state, maintenances) {
    Object.assign(state, { maintenances });
  },
  [SET_CAR_ERP_INFORMATION](state, erpinformation) {
    Object.assign(state, { erpinformation });
  },
  [SET_CAR_ERP_OWNER](state, erpowner) {
    Object.assign(state, { erpowner });
  },
  [SET_CAR_ERP_INSURANCES](state, erpinsurance) {
    Object.assign(state, { erpinsurance });
  },
  [SET_CAR_ERP_TAXES](state, erptaxes) {
    Object.assign(state, { erptaxes });
  },
  [SET_CAR_ERP_ACTS](state, erpact) {
    Object.assign(state, { erpact });
  },
  [SET_CAR_ERP_OPTIONS](state, erpoption) {
    Object.assign(state, { erpoption });
  },
  [SET_CAR_ERP_MAINTENANCE](state, erpmaintenances) {
    Object.assign(state, { erpmaintenances });
  },




};

