import {
  SET_CARS,
  SET_CAR,
  SET_CAR_TYPES,
  SET_CAR_INFORMATION,
  SET_CAR_OWNER,
  SET_CAR_INSURANCES,
  SET_CAR_TAXES,
  SET_CAR_ACTS,
  SET_CAR_OPTIONS,
  SET_CAR_MAINTENANCE,
  SET_CAR_ERP_INFORMATION,
  SET_CAR_ERP_OWNER,
  SET_CAR_ERP_INSURANCES,
  SET_CAR_ERP_TAXES,
  SET_CAR_ERP_ACTS,
  SET_CAR_ERP_MAINTENANCE,
  SET_CAR_ERP_OPTIONS,


} from './mutations'

export default {
  async get({ commit }, { search= '', status = null, type = '', report = false } = {}) {
    commit(SET_CARS, [])
    const { data } = await this.$axios.get('cars', {
      params: { status, search, type, report }
    })
    commit(SET_CARS, data)
  },
  async getCarFree({ commit }, {startTime = '' , endTime = '' } = {}) {
    commit(SET_CARS, [])
    const { data } = await this.$axios.get('/cars/free', {
      params: { startTime, endTime }
    })
    commit(SET_CARS, data)
  },
  async find({ commit }, id) {
    commit(SET_CAR, null)

    const { data } = await this.$axios.get(`cars/${id}`)
    commit(SET_CAR, data)
  },
  async findInformation({ commit }, id) {
    commit(SET_CAR_INFORMATION, null)

    const { data } = await this.$axios.get(`cars/${id}/information`)
    commit(SET_CAR_INFORMATION, data)
  },
  async findOwner({ commit }, id) {
    commit(SET_CAR_OWNER, null)

    const { data } = await this.$axios.get(`cars/${id}/information/owner`)
    commit(SET_CAR_OWNER, data)
  },
  async findInsurances({ commit }, id) {
    commit(SET_CAR_INSURANCES, [])

    const { data } = await this.$axios.get(`cars/${id}/information/insurances`)
    commit(SET_CAR_INSURANCES, data)
  },
  async findTaxes({ commit }, id) {
    commit(SET_CAR_TAXES, [])

    const { data } = await this.$axios.get(`cars/${id}/information/taxes`)
    commit(SET_CAR_TAXES, data)
  },
  async findActs({ commit }, id) {
    commit(SET_CAR_ACTS, [])

    const { data } = await this.$axios.get(`cars/${id}/information/acts`)
    commit(SET_CAR_ACTS, data)
  },
  async findMaintenance ({ commit }, id) {
    commit(SET_CAR_MAINTENANCE, [])

    const { data } = await this.$axios.get(`cars/${id}/information/maintenance`)
    commit(SET_CAR_MAINTENANCE, data)
  },
  async findOptions({ commit }, id) {
    commit(SET_CAR_OPTIONS, [])

    const { data } = await this.$axios.get(`cars/${id}/information/options`)
    commit(SET_CAR_OPTIONS, data)
  },
  async getType({ commit }) {
    commit(SET_CAR_TYPES, [])

    const { data } = await this.$axios.get('setup/car-types')
    commit(SET_CAR_TYPES, data)
  },
  async findERPInformation({ commit }, id) {
    commit(SET_CAR_ERP_INFORMATION, null)
    const { data } = await this.$axios.get(`erp/cars/${id}/information`)
    commit(SET_CAR_ERP_INFORMATION, data)
  },
  async findERPOwner({ commit }, id) {
    commit(SET_CAR_ERP_OWNER, null)

    const { data } = await this.$axios.get(`erp/cars/${id}/information/owner`)
    commit(SET_CAR_ERP_OWNER, data)
  },
  async findERPInsurances({ commit }, id) {
    commit(SET_CAR_ERP_INSURANCES, [])

    const { data } = await this.$axios.get(`erp/cars/${id}/information/insurances`)
    commit(SET_CAR_ERP_INSURANCES, data)
  },
  async findERPTaxes({ commit }, id) {
    commit(SET_CAR_ERP_TAXES, [])

    const { data } = await this.$axios.get(`/erp/cars/${id}/information/taxes`)
    commit(SET_CAR_ERP_TAXES, data)
  },
  async findERPActs({ commit }, id) {
    commit(SET_CAR_ERP_ACTS, [])

    const { data } = await this.$axios.get(`erp/cars/${id}/information/acts`)
    commit(SET_CAR_ERP_ACTS, data)
  },
  async findERPOptions({commit},id){
    commit(SET_CAR_ERP_OPTIONS, [])

    const { data } = await this.$axios.get(`erp/cars/${id}/information/options`)
    commit(SET_CAR_ERP_OPTIONS, data)
  },
  async findERPMaintenance ({ commit }, id) {
    commit(SET_CAR_ERP_MAINTENANCE, [])

    const { data } = await this.$axios.get(`erp/cars/${id}/information/maintenance`)
    commit(SET_CAR_ERP_MAINTENANCE, data)
  },
  createInformation(_, body) {
    return this.$axios.post('car/info', body)
  }


}
