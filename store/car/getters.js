export default {
  cars: ({ cars }) => cars,
  car: ({ car }) => car,
  carTypes: ({ carTypes }) => carTypes,
  information: ({ information }) => information,
  owner: ({ owner }) => owner,
  insurances: ({ insurances }) => insurances,
  taxes: ({ taxes }) => taxes,
  acts: ({ acts }) => acts,
  options: ({ options }) => options,
  types: ({ cars }) =>
    cars
      .map(({ type }) => type)
      .reduce((carry, current) => {
        if (!carry.find(({ typeId }) => typeId === current.typeId)) {
          carry.push(current);
        }
        return carry;
      }, []),
  maintenances: ({ maintenances }) => maintenances,
  erpinformation : ({erpinformation}) => erpinformation,
  erpmaintenances: ({ erpmaintenances }) => erpmaintenances,
  erpinsurance: ({ erpinsurance }) => erpinsurance,
  erptaxes: ({ erptaxes }) => erptaxes,
  erpact: ({ erpact }) => erpact,

};
