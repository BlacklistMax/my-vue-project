export default () => ({
  cars: [
    {1:null}
  ],
  car: null,
  carTypes: [],
  information: null,
  owner: null,
  insurances: [],
  taxes: [],
  acts: [],
  maintenances: [],
  options: [],
  erpinformation:[],
  erpowner:[],
  erpinsurance:[],
  erptaxes: [],
  erpact:[],
  erpoption:[],
  erpmaintenances: []
});
