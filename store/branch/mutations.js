export const SET_BRANCHES = 'SET_BRANCHES'
export const SET_BRANCH = 'SET_BRANCH'

export default {
  [SET_BRANCHES] (state, branches) {
    Object.assign(state, { branches })
  },
  [SET_BRANCH] (state, branch) {
    Object.assign(state, { branch })
  }
}
