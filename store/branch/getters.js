export default {
  branches: ({ branches }) => branches,
  branch: ({ branch }) => branch,
}
