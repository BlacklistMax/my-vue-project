import { SET_BRANCHES, SET_BRANCH } from "./mutations";

export default {
  async get({ commit }, { companyId = null, status = null } = {}) {
    const { data } = await this.$axios.get("branches", {
      params: { companyId, status }
    });

    commit(SET_BRANCHES, data);
  },
  async admin_get({ commit }, { companyId = null, status = null } = {}) {
    const { data } = await this.$axios.get("/admin/branch", {
      params: { companyId, status }
    });

    commit(SET_BRANCHES, data);
  },
  create(_, body) {
    return this.$axios.post("branches", body);
  },
  async find({ commit }, id) {
    const { data } = await this.$axios.get(`branches/${id}`);
    commit(SET_BRANCH, data);
  },
  update(_, { id, name, responsibleUserId }) {
    return this.$axios.patch(`branches/${id}`, {
      name,
      responsibleUserId
    });
  },
  remove(_, id) {
    return this.$axios.delete(`branches/${id}`);
  }
};
