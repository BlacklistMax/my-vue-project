export default {
  title: ({ title }) => title,
  buttons: ({ buttons }) => buttons,
  isLoginLoading: ({ isLoginLoading }) => isLoginLoading,
  permissions: ({ permissions }) => permissions,
}
