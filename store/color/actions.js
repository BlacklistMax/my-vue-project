import { SET_COLORS } from './mutations'

export default {
  async get({ commit }, { status = null } = {}) {
    const { data } = await this.$axios.get('setup/colors', {
      params: { status }
    })
    commit(SET_COLORS, data)

    return data
  },
  update(_, colors) {
    return this.$axios.put('setup/colors', { colors })
  },
}
