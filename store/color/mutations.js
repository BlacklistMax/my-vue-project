export const SET_COLORS = 'SET_COLORS'

export default {
  [SET_COLORS] (state, colors) {
    Object.assign(state, { colors })
  },
}
