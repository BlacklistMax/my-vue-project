import { SET_CONTRACTS, SET_CONTRACT } from './mutations'

export default {
  async get({ commit }) {
    const { data } = await this.$axios.get('contracts')
    commit(SET_CONTRACTS, data)
  },
  create(_, body) {
    return this.$axios.post('contracts', body)
  },
  async find({ commit }, id) {
    const { data } = await this.$axios.get(`contracts/${id}`)
    commit(SET_CONTRACT, data)
  },
  update(_, body) {
    return this.$axios.patch(`contracts/${body.id}`, body)
  },
  remove(_, id) {
    return this.$axios.delete(`contracts/${id}`)
  }
}
