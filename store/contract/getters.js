export default {
  contracts: ({ contracts }) => contracts,
  contract: ({ contract }) => contract,
}
