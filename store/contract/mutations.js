export const SET_CONTRACTS = 'SET_CONTRACTS'
export const SET_CONTRACT = 'SET_CONTRACT'

export default {
  [SET_CONTRACTS] (state, contracts) {
    Object.assign(state, { contracts })
  },
  [SET_CONTRACT] (state, contract) {
    Object.assign(state, { contract })
  }
}
