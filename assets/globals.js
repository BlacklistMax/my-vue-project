export const IS_PRODUCTION = true

export const HOST_API = IS_PRODUCTION
  ? 'http://vms.siamraj.com'
  : 'http://127.0.0.1:8081'

export const FORM_TYPES = {
  USER_LOGIN: 'USER_LOGIN',
  ADMIN_LOGIN: 'ADMIN_LOGIN',
}

export const USERS = {
  CHANNEL: 'email',
}

export const PRODUCTS = {}

export const DELAYS = {
  START_COUNTDOWN: 0,
  COUNTDOWN: 1000,
}

export const MONTHS_TH = [
  'มกราคม',
  'กุมภาพันธ์',
  'มีนาคม',
  'เมษายน',
  'พฤษภาคม',
  'มิถุนายน',
  'กรกฎาคม',
  'สิงหาคม',
  'กันยายน',
  'ตุลาคม',
  'พฤศจิกายน',
  'ธันวาคม',
]

export const SHORT_MONTHS_TH = [
  'ม.ค.',
  'ก.พ.',
  'มี.ค.',
  'เม.ย.',
  'พ.ค.',
  'มิ.ย.',
  'ก.ค.',
  'ส.ค.',
  'ก.ย.',
  'ต.ค.',
  'พ.ย.',
  'ธ.ค.',
]

export const MONTHS_EN = [
  'January',
  'February ',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
]

export const SHORT_MONTHS_EN = [
  'Jan.',
  'Feb.',
  'Mar.',
  'Apr.',
  'May.',
  'Jun.',
  'Jul.',
  'Aug.',
  'Sep.',
  'Oct.',
  'Nov.',
  'Dec.',
]

export const FORMATS = {
  SHORT_MONTH: 'short_month',
  SHORT_MONTH_AND_YEAR: 'short_month_and_year',
  DATE_FORMAT: 'yyyy-MM-dd',
  TIME_FORMAT: 'HH:mm:ss',
  TIME_NO_SECOND_FORMAT: 'HH:mm',
}

export const CARS = {
  TYPES: {
    WAITING_APPROVE: 'waiting_approve',
    USING: 'using',
    HISTORY: 'history',
  },
  STATUS_IDS: {
    WAITING_APPROVE: 1,
    REPAIR: 2,
    SUCCESS: 3,
    NO_APPROVE: 4,
    CANCEL: 5
  },
}

export const COLORS = {
  MAIN_CI: '#E79918',
}
