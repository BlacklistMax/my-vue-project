import colors from "vuetify/es5/util/colors";

export default {
  target: "static",
  ssr: false, // on production set to false for static host
  generate: { fallback: true },
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: "%s - Siam Rajathanee",
    title: "Siam Rajathanee",
    htmlAttrs: {
      lang: "en"
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" }
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/Favicon_SO.png" }]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    "@/assets/scss/main.scss",
    "@/assets/scss/responsive.scss",
    "@/assets/scss/base.scss"
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    "~/plugins/vue-ctk-date-time-picker.js",
    "~/plugins/v-mask.client.js"
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/vuetify
    "@nuxtjs/vuetify",
    "@nuxtjs/google-fonts"
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    "@nuxtjs/axios",
    "@nuxtjs/auth-next",
    "@nuxtjs/toast",
    "@nuxtjs/dayjs"
  ],

  googleFonts: {
    families: {
      Prompt: true,
      Sarabun: true,
      "Material Icons": true
    }
  },

  toast: {
    theme: "toasted-primary",
    position: "top-center",
    duration: 5000,
    register: [
      // Register custom toasts
      {
        name: "error",
        message: obj => obj.message,
        options: {
          type: "error",
          className: "error-wrapper",
          icon: "highlight_off",
          action: {
            text: "X",
            onClick: (e, toastObject) => {
              toastObject.goAway(0);
            }
          },
          keepOnHover: true
        }
      },
      {
        name: "success",
        message: obj => obj.message,
        options: {
          type: "success",
          className: "success-wrapper",
          icon: "task_alt",
          action: {
            text: "X",
            onClick: (e, toastObject) => {
              toastObject.goAway(0);
            }
          },
          keepOnHover: true
        }
      },
      {
        name: "warning",
        message: obj => obj.message,
        options: {
          type: "info",
          className: "warning-wrapper",
          icon: "warning_amber",
          action: {
            text: "X",
            onClick: (e, toastObject) => {
              toastObject.goAway(0);
            }
          },
          keepOnHover: true
        }
      },
      {
        name: "info",
        message: obj => obj.message,
        options: {
          type: "info",
          className: "info-wrapper",
          icon: "error_outline",
          action: {
            text: "X",
            onClick: (e, toastObject) => {
              toastObject.goAway(0);
            }
          },
          keepOnHover: true
        }
      }
    ]
  },

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    baseURL:
      process.env.NODE_ENV !== "production"
        ? "http://127.0.0.1:9900/api"
        : "https://vms-api.siamrajathanee.dev/api",
            // process.env.API_URL: "http://vms.siamraj.com/api/api",
    credentials: true
  },

  auth: {
    strategies: {
      local: {
        endpoints: {
          login: {
            method: "post",
            url: "login",
            propertyName: "token"
          },
          user: {
            method: "get",
            url: "me",
            propertyName: "user"
          },
          logout: false
        }
      }
    }
  },

  router: {
    middleware: ["auth", "unsetButton"]
  },

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ["~/assets/variables.scss"],
    treeShake: true,
    theme: {
      dark: false,
      themes: {
        light: {
          error: colors.red.darken2
        },
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          // error:colors.red.darken2,
          success: colors.green.accent3
        }
      }
    }
  },

  dayjs: {
    locales: ["en", "th"],
    defaultLocale: "th",
    defaultTimeZone: "Asia/Bangkok",
    plugins: ["utc", "timezone"]
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},
};
