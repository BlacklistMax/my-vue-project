import { UNSET_BUTTONS } from "@/store/mutations"

export default ({ store }) => {
  store.commit(UNSET_BUTTONS)
}
