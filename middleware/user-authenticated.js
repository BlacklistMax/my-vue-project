export default function ({ store, redirect }) {
  if (!store.$auth.user || (store.$auth.user && !store.$auth.user.userId)) {
    return redirect('/login')
  }
}
